# 概述
- 这个仓库包含一个带有注释反汇编的魂斗罗美版NESROM,并包含将汇编代码重新装配成游戏的脚本.
- 另外附赠了额外的文档,流程图,脚本和工具用来进一步研究魂斗罗这个游戏

- 特别感谢`Trax`,基于他的翻译项目[Revenge of the Red
Falcon](https://www.romhacking.net/hacks/2701/),才会有这个项目的产生

```
|-- docs - 附赠文档
|   |-- attachments - 其他文档中的图片视频
|   |-- diagrams - 游戏流程图
|   |-- lua_scripts - 模拟器(mesen/fceux)的lua脚本
|   |-- sprite_library - 提取的精灵图
|-- src - 游戏源代码
|   |-- assets - 压缩图片数据和音频编码
|-- assets.txt - 资源列表,baserom.nes中偏移位置和长度
|-- build.bat - win的cmd编译脚本
|-- build.ps1 - win的powershell的编译脚本
|-- build.sh - linux/mac的编译脚本
|-- contra.cfg - cc65配置
|-- README.md
|-- set_bytes.vbs - 从baserom.nes中提取数据的脚本
```
# 构建流程

## 前置素材
  * 仓库中并没有包含编译生成游戏所需要的的数据(图片数据和音频数据).需要存在一个美国NES版本的魂斗罗`contra`名为`baserom.nes`用来提取需要的素材,rom的md5编码`7BDAD8B4A7A56A634C9649D20BD3011B`.
  * 下载cc65汇编和链接软件[cc65编译套件](https://cc65.github.io/). 安装并添加bin目录到系统环境.

## 说明
 * 仓库包含3个构建脚本.只需要执行对应脚本即可.
```
.\build.ps1 <-- Windows
.\build.bat <-- Windows (no PowerShell)
./build.sh <-- Unix
```

* `build.ps1` - PowerShell script recommended for building on Windows machines.
* `build.bat` - bat script that can be used on windows machines without
  PowerShell, but requires VBScript support.
* `build.sh` - bash script to be used in unix environments, or on Windows
  environments with bash support (Git bash, WSL, etc.)

## 文档
- 附加文档包含了代码中有趣功能的解释.下面是一些比较重要的文档

* `docs/Aim Documentation.md` -敌人动画机制
* `docs/Bugs.md` - 反汇编重现的bugs
* `docs/Contra Control Flow.md` - 游戏关卡和流程详情.
* `docs/Enemy Glossary.md` - 敌人类型
* `docs/Enemy Routines.md` - 敌人关卡配置和随机生成.
* `docs/Graphics Documentation.md` - 图形相关分析.
* `docs/Sound Documentation.md` - 音频引擎的分析.

- 所有提取出的精灵素材`docs/sprite_library/README.md`

### 起步

- `src`目录下包含根据内存bank切分的. 魂斗罗卡带的Mapper是
[UxROM](https://www.nesdev.org/wiki/UxROM). 这个游戏包含8个16kb的bank,总共128kb.
- Banks 0到在内存`$8000-$bfff`切换,Bank7是常驻内存`$c000-$ffff`. 
- 同一时间只有2个Banks存在于内存中,是理解游戏的观点点.  
- 和大多数游戏引擎一样游戏从`bank7.asm`开始,
- 包含3个NES中断,并且常驻内存.

- 建议从`docs/Contra Control Flow.md`开始阅读. 该文件包含了游戏整体循环流程.

* `bank0.asm` - 敌人的行为逻辑控制: AI,运动,攻击魔兽,血量等.大部分敌人都是在这个bank,
而在多个关卡出现的敌人在`bank7.asm`.
* `bank1.asm` - 音频和精灵处理,音频占据了1/4,剩下的事精灵数据和绘制精灵数据的代码.
* `bank2.asm` - 关卡地图RLE压缩数据.包含了Tile数据和相关的属性表数据.包含根据玩家状态设置玩家精灵的控制,每个关卡的定制信息,
  比如敌人的刷新等.
* `bank3.asm` - 颜色表数据的处理,以及关卡结束的处理.
* `bank4.asm` - 结束的动画与致辞数据.
* `bank5.asm` - 压缩数据,以及demo关卡.
* `bank6.asm` - 压缩数据,文本相关的,以及武器和子弹相关.
* `bank7.asm` - 游戏逻辑核心,3个中断处理,游戏入了.包含命名表和精灵的绘制,bank切换,简介片段，输入控制,积分计算,图形解压等主要功能.

### 备注Annotations
- 在分析汇编，使用了以下标记,.

* `!(BUG?)` - BUG
* `!(HUH)` - 无法确定的代码
* `!(WHY?)` - 不理解的逻辑 
* `!(UNUSED)` - 未使用的 
* `!(OBS)` - 重点笔记

# 项目历史 Project History
* 是在观看了
[Summoning Salt](https://www.youtube.com/channel/UCtUbO6rBht0daVIOGML3c8w)频道的
[The History of Contra World Records](https://www.youtube.com/watch?v=GgOE64kgjjo).
在视频35.19处,一个叫`dk28`的速通玩家在关卡1中死去,却重生后前进到关卡1.这个视频解释了这是如何发生的.  
因此我开始对汇编代码感兴趣开始了这个工程.

魂斗罗NES使用反汇编工具进行反汇编后得到的汇编代码.作为代码分析的起点,
但是将很多代码行代码看着数据,必须手动修正.  每个bank分为一个`.asm`文件.

使用`ps1`或者`bat`脚本,可以调用ca65将`.asm`编译为`.o`文件,然后使用`cl65`链接生成NES的ROM文件
`contra.cfg`配置了内存和bank的相关信息.

然后根据`Trax`的反汇编进行注释修正.

# 构建流程

* 构建脚本包含以下任务:
 * 从`baserom.nes`提前素材事件到`src/assets`
 * 将每个bank的`.asm`文件编译为`.o`文件
 * 将`constants.asm`和`ines_header.asm`编译成`.o`文件
 * 将所有`.o`文件链接生成一个`.nes`ROM文件

* 构建脚本使用`cc65编译套件`[cc65 compiler suite](https://cc65.github.io/).
素材数据根据`assets.txt`从`baserom.nes`提取,`assets.txt`的每一行定义了素材文件的名称和偏移量以及长度.

## 1.汇编.
每个`.asm`文件编译成一个`.o`文件,下面是这个过程的主要处理内容
### 导入导出验证
汇编中,`.import`指令中未定义的符号将会产生错误提示.

## 2. 链接
通过cl65连接器将`.o`文件合并成一个`.nes`ROM文件.替换label为内存地址..
`contra.cfg`包含内存分配和代码段分配.

### Contra.cfg内存分配
跳转标签的定义
### Contra.cfg代码段分配
`.o`文件的链接顺序.

# 参考
 * Trax's [Disassembly of _Contra (US)_](https://www.bwass.org/romhack/contra/)
 * Trax's [documentation on romhacking.net](https://www.romhacking.net/documents/713/)
 * [Tomorrow Corporation's Retro Game Internal Series on Contra](http://tomorrowcorporation.com/posts/retro-game-internals)
 * [Overview of NES Rendering by Austin Morlan](https://austinmorlan.com/posts/nes_rendering_overview/)
 * [Rom Detective article on Contra](http://www.romdetectives.com/Wiki/index.php?title=Contra_(NES))
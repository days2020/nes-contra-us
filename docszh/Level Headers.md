# Overview
魂斗罗会从bank2加载关卡相关的配置,这些配置称为关卡头部描述信息.
每个关卡使用32字节作为关卡的描述信息,比如关卡是室内还是室外,是水平还是垂直的等.

代码使用`leavel_headers`标签,每个关卡对应`level_x_header`标签.
```
levelHeader {
  locationType:             byte, //关卡类型 室内,室外,室内boss
  scrollType:               byte, //户外关卡滚动类型 水平滚动,垂直滚动
  superTilePointer:         byte[2], //超级贴图块地址
  superTileData:            byte[2], //超级贴图块数据组成
  superTilePaletteData:     byte[2],//超级贴图块颜色表
  alternateGraphics:        byte, //其他额外数据
  tileCollisionLimits:      byte[3],//code1,code2,code
  cyclingPalltte:           byte[4],//颜色表索引
  backgroudInitialColor:    byte[4],//背景初始化颜色
  spriteInitialColor:       byte[4],//精灵初始化颜色
  scrollStopScreen:         byte,
  solidBackgroundCollision: byte,
  unused:                   byte[6] 未使用
}:byte[32]
```
# 关卡类型 Byte Offset 0 - Location Type

  * Offset: 0
  * Memory Location: $40
  * Variable Name: `LEVEL_LOCATION_TYPE`
第一个字节可能的值包含3种

  * Outdoor - #$00
  * Indoor/Base - #$01
  * Indoor Boss - #$80

户外关卡#$00是经典水平或者垂直滚轴的平台跳跃游戏,
室内关卡#$01是关卡2和4特定的基地关卡
关卡2和4将会设置为#$80,


# 户外关卡滚动方向 Byte Offset 1 - Outdoor Scrolling Type

  * Offset: 1
  * Memory Location: $41
  * Variable Name: `LEVEL_SCROLLING_TYPE`

户外关卡的滚动方式 包含水平和垂直.

  * Horizontal - #$00
  * Vertical - #$01

# Byte Offsets 2 and 3 - Screen Super-Tile Pointer

  * Offset: 2 and 3
  * Memory Location: 2-byte address $42 and $43
  * Variable Name: `LEVEL_SCREEN_SUPERTILES_PTR`
第3和第4字节指向每个关卡屏幕的数据在bank2中的地址,

比如关卡指向`level_3_supertiles_screen_ptr_table`,包含关卡的每个屏幕的数据地址指针
每个屏幕是256x256像素网格,因为关卡3是一个垂直关卡,每个屏幕64的超级贴图块组成。
仔细查看`level_3_supertiles_screen_01`处的数据,将会发现RLE编码的字节指定了超级贴图块的索引
解压后对应到`level_3_supertile_data`的偏移量.

# Byte Offsets 4 and 5 - Super-Tile Data

  * Offset: 4 and 5
  * Memory Location: 2-byte address $44 and $45
  * Variable Name: `LEVEL_SUPERTILE_DATA_PTR`

第4和5是指向使用的超级贴图块数据的指针.包含模式表的pattern table tile的索引，
比如关卡3的`level_3_supertile_data`

# Byte Offsets 6 and 7 - Super-Tile Palette Data

  * Offset: 6 and 7
  * Memory Location: 2-byte address $46 and $47
  * Variable Name: `LEVEL_SUPERTILE_PALETTE_DATA`

第6和7是关卡使用的颜色表数据，
关卡使用的颜色表数据,比如`level_2_palette_data`

# Byte Offset 8 - Alternate Graphics Loading Location

  * Offset: 8
  * Location: $48
  * Variable Name: `LEVEL_ALT_GRAPHICS_POS`

This byte specifies the alternate graphics loading location.  This is the screen
where the game has the ability to change out the pattern table tiles for a
level.

# Byte Offsets 9, 10, and 11 - Tile Collision Limits

  * Offset: 9, 10, and 11
  * Location: $49, $4a, $4b
  * Variable Name: `COLLISION_CODE_1_TILE_INDEX`, `COLLISION_CODE_0_TILE_INDEX`,
    and `COLLISION_CODE_2_TILE_INDEX` (in that order)

指定碰撞配置.

These three bytes are used to specify which pattern table tiles are which
collision codes.

* Pattern table tiles below the value in $49 (excluding #$00) are considered
Collision Code 1 (floor).
* Pattern table tiles >= the value in $49, but less than the value in $4a are
considered Collision Code 0 (empty)
* Pattern table tiles >= $4a but less than this tile index are considered
Collision Code 2 (water)
* Pattern table tiles above the last entry, are considered Collision Code 3
(Solid).

# Byte Offset 12, 13, 14, and 15 - Cycling Palette Colors

  * Offset: 12, 13, 14, and 15
  * Location: $4c, $4d, $4e, and $4f
  * Variable Name: `LEVEL_PALETTE_CYCLE_INDEXES`

Each level natively supports having its 4th nametable (background) palette cycle
through up to 4 different sets of colors, where each set of colors is
represented by an index into the `game_palettes` table.  This header section
contains those specific indexes to cycle through.

For more details see `Graphics Documentation.md`. There is a section titled
`Palette -> Cycling`.

# Byte Offset 16, 17, 18, and 19 - Nametable Palette Initial Colors

  * Offset: 16, 17, 18, and 19
  * Location: $50, $51, $52, and $53
  * Variable Name: `LEVEL_PALETTE_INDEX`

Initial background tile palette colors for the level. Indexes into
`game_palettes`.

# Byte Offset 20, 21, 22, and 23 - Nametable Palette Initial Colors

  * Offset: 20, 21, 22, and 23
  * Location: $54, $55, $56, and $57
  * Variable Name: `LEVEL_PALETTE_INDEX`

Initial sprite tile palette colors for the level. Indexes into `game_palettes`.

# Byte Offset 24 - Scroll Stop Screen

  * Offset: 24
  * Location: $58
  * Variable Name: `LEVEL_STOP_SCROLL`

The screen of the level to stop scrolling. The value in memory is set to #$ff
when boss auto scroll starts

# Byte Offset 25 - Solid Background Collision

  * Offset: 25
  * Location: $59
  * Variable Name: `LEVEL_SOLID_BG_COLLISION_CHECK`

Specifies whether to check for bullet and weapon item solid bg collisions.

  1. When non-zero, specifies that the weapon item should check for solid bg
     collisions (see `weapon_item_check_bg_collision`)
  2. When negative (bit 8 set), specifies to let bullets for both players and
     enemies to check for bullet-solid background collisions.  This is used on
     levels 6 - energy zone and 7 - hangar. (see
     `check_bullet_solid_bg_collision` and `enemy_bullet_routine_01`)

# Byte Offset 26 through 32 - Unused

The last 6 bytes in each level's level header are unused and set to #$00.
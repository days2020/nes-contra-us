# 概述 Overview

敌人逻辑行为由敌人子程序控制,每个敌人包含一系列可执行的子程序,
在bank7的`*_routine_ptr_tbl`指定了每个敌人的子程序
在`enemy_routine_ptr_tbl`中指定了关卡每个屏幕生成的敌人类型
每个关卡特定的在表`enemy_routine_level_x`中指定.
关卡2和4则共享表`enemy_routine_level_2_4`.

在`level_routine_04`和`level_routine_0a`子程序执行过程中,每个类型敌人的执行相应的逻辑.
在`exe_all_enemy_routine`遍历`ENEMY_ROUTINE`#$f to #$0
- 通用敌人行为
- 关卡特定敌人行为
- 
# 生成 Generation
敌人生成有两种方式 一种是随机生成,一种是关卡特定硬编码
Enemies can be generated in the level in one of two ways: random generation, and
level-specific hard-coded locations.

# 关卡敌人 Level Enemies
每个关卡都有屏幕中敌人生成位置的硬编码信息,这些敌人是关卡固定生成的.
在bank2的`level_enemy_screen_ptr_ptr_tbl`定义,
每个关卡使用2个字节,包含8x2个字节作为描述入口.
比如`level_1_enemy_screen_ptr_tbl`.  每2个字节存储作为另一个表的地址
比如`level_1_enemy_screen_02`.  包含了单个屏幕的敌人.  
屏幕0不包含敌人,第一个描述关联到第二屏,每个关卡包含多个屏幕描述.

```
//地址
addr:byte[2]

level_enemy_screen_ptr_ptr_tbl{
    level_x_enemy_screen_ptr_tbl{
        level_x_enemy_screen_yy{
            enemy_data:byte[3][n]
            end:byte
        }:addr[] //特定屏的敌人数据地址
    }:addr[],//每个关卡的敌人数据地址
}:addr[8]

enemy_destroyed_routine_ptr_tbl{
    enemy_destroyed_routine_00{

    }:addr[]
}:addr[8]
```

## 数据结构 Data Structure

### 户外关卡 Outdoor levels
每个屏幕描述中,使用3个字节描述一个敌人，
比如`level_1_enemy_screen_00`中的第一个敌人的数据是`.byte $10,$05,$60`
描述了一个不会射击,向左走的战士,3个字节需要拆分为bit为来进一步理解

```
0001 0000  0000 0101  0110 0000
XXXX XXXX  RRTT TTTT  YYYY YAAA
```

* `X` - X offset 
* `R` - Repeat
* `T` - Enemy Type
* `Y` - Y Offset
* `A` - Enemy Attribute

### Byte 1 - XX byte 敌人x轴的位置信息
The first byte, #$10, from the example above specifies the x position of the
enemy.

### Byte 2 - Repeat and Enemy Type
敌人重复是否和类型

下面是一个包含重复的敌人描述
```
level_1_enemy_screen_09:
    .byte $10,$43,$40,$b4 ; flying capsule (enemy type #$03), attribute: 000 (R), location: (#$10, #$40)
                          ; repeat: 1 [(y = #$b0, attr = 100)]
    .byte $e0,$07,$81     ; red turret (enemy type #$07), attribute: 001, location: (#$e0, #$80)
    .byte $ff
```

### Byte 3 - Y Offset and Attribute
Y轴位置和敌人属性信息，
第三字节的#$60包含Y轴信息和敌人属性。 前面的5位#$05描述Y轴位置,剩下3位描述敌人属性
关于敌人的属性分析见`Enemy Glossary.md`.

### 室内关卡 Indoor/Base Levels

```
XXXX YYYY CDTT TTTT AAAA AAAA
```

* `X` - X offset
* `Y` - Y Offset
* `C` - Whether or not to add #$08 to Y position
* `D` - Whether or not to add #$08 to X position
* `T` - Enemy Type
* `A` - Enemy Attribute

# 敌人的销毁 Enemy Destroyed

敌人死亡后会执行由`enemy_destroyed_routine_ptr_tbl`指定的`enemy_destroyed_routine_xx`子程序.

比如战士死亡后会执行`enemy_destroyed_routine_01`指定的`soldier_routine_04`

# 战士随机生成 Soldier Generation
除了硬编码的固定位置生成,魂斗罗会随机生成敌人.
当游玩关卡时,在`level_routine_04`设置的游戏状态`game_routine_05`,会决定是否生成战士,
会在bank2的子程序`exe_soldier_generation`中设置

`exe_soldier_generation`根据`SOLDIER_GENERATION_ROUTINE`执行3种战士生成的其中一种.
默认的的值是#$00,所以执行`soldier_generation_00` .

## soldier_generation_00
`soldier_generation_00`初始化一个关卡特定的定时器用来控制战士生成的速度.
定时器会根据游戏进化的时间和玩家当前武器的强度进行动态调整.

比如关卡3,默认的关卡定时器数值是#$d8(`level_soldier_generation_timer`表中指定)
如果玩家完成一次游戏并且使用S弹,怎定时器会调整为#$a1.

```
#$a1 = #$d8 - (#$01 * #$28) - (#$05 * #$03)
```

Soldier generation is disabled on the indoor/base levels (level 2 and level 4)
along with level 8 (alien's lair).  They are disabled by a value of #$00 being
specified in the `level_soldier_generation_timer` table.  For these levels, no
other soldier generation routine will be run, only `soldier_generation_00`.

Once the soldier generation timer has been initialized and adjusted, the
`SOLDIER_GENERATION_ROUTINE` is incremented so that the next game loop's
`exe_soldier_generation` causes `soldier_generation_01` to execute.

## 查询生成的x,y位置 soldier_generation_01

`soldier_generation_01` is responsible for decrementing the soldier generation
timer until it elapses. Then it is responsible for creating the soldier, if
certain conditions are met. This includes randomizing the soldier's location and
enemy attributes.

`soldier_generation_01` will first look at the current soldier generation timer,
if the timer is not yet less than #$00, then the timer is decremented by #$02,
unless the frame is scrolling on an odd frame number. Then the timer is only
decremented by #$01.

Once the soldier generation timer has elapsed, the routine looks for an
appropriate location to generate the soldier on the screen.  Soldiers are
always generated from the left or right edge of the screen.  First the starting
horizontal position is determined. This is essentially determined randomly by
the current frame number and values in the `gen_soldier_initial_x_pos` table.
The result will be either the left edge (#$0a) or the right edge (#$fa or #$fc).

There is an exception for level one.  Until a larger number of soldiers have
already been generated, soldiers will only appear from the right, probably to
make the beginning of the game slightly easier.

Once the x position is decided, the routine will start looking for a vertical
location that has a ground for the soldier to stand on.  It does this in one of
3 ways randomly to ensure soldiers are generated from multiple locations if
possible.  The 3 methods are from top of the screen to the bottom, from the
bottom of the screen to the top, and from the player vertical position up to the
top.

If a horizontal and vertical position is found where a soldier can be placed on
the ground, then some memory is updated to specify the location and the soldier
generation routine is incremented to `soldier_generation_02`.

## 敌人初始化 soldier_generation_02

At this point, a location is found for the soldier to generate.
`soldier_generation_02` is responsible for actually initializing and creating
the soldier.  Some checks are performed to make sure it's appropriate to
generate a soldier, for example, when `ENEMY_ATTACK_FLAG` is set to #$00 (off),
then a soldier will not be generated. Other checks include that there are no
solid blocks (collision code #$80) right in front of the soldier to generate,
and that there is no player right next to the edge of the screen where the
soldier would be generated from (this check doesn't happen after beating the
game at least once).  If any checks determine that the soldier should not be
generated, then the routine resets the `SOLDIER_GENERATION_ROUTINE` back to #$00
and stops.

To randomize the various behaviors of the generated soldiers, this routine will
look up initial behavior from one of the `soldier_level_attributes_xx` tables
based on the level. This will randomize the soldier direction, whether or not
the soldier will shoot and how frequently, and a value specifying the
probability of ultimately not generating the soldier.  Finally, the soldier is
generated and the values are moved into the standard enemy memory location
addresses, creating the soldier.
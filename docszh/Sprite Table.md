; each sprite entry is defined as follows
每个精灵都是如下定义的
```
SpriteTile{
    number:byte,  //精灵的贴图数量
    block{
        yPosition:byte,//相对左上角Y位置
        index:byte, //贴图索引 奇数从精灵表读取 偶数从背景表读取
        attr:byte,  //精灵属性
        xPositon:byye //相对左上角X位置
    }:byte[4], //8x16的贴图块
}
```
; first byte (n) is number of tiles for sprite
第一个byte是精灵贴图的数量
第二个byte是贴图索引，
    奇数的读取精灵贴图表
    偶数的读取背景贴图表
第三个byte是绘制属性
第三个byte是x轴相对位置
; followed by n 4-byte groups
; each 4-byte group defines a block that is 2 tiles tall (8x16 pixels)
; * byte 0: Y position in pixels (relative to sprite base position (SPRITE_Y_POS), can be negative)
; * byte 1: pattern table tile index/code
;   * if even, tile is pulled from 0x0000 (sprite section) (left pattern table)
;   * if odd, tile is pulled from 0x1000 (background section) (right pattern table)
;   * one byte is for both tiles (8x16)
;     * 1st tile is byte specified
;     * 2nd tile is (byte + 1), i.e. the tile immediately after the one specified
; * byte 2: palette code, flipping, drawing priority (foreground or background)
;     * x... .... - Vertical flip
;     * .x.. .... - Horizontal flip
;     * ..x. .... - Drawing priority (whether to draw behind background or not)
;     * .... ..xx - Palette code
; * byte 3: X position in pixels (relative to sprite position (SPRITE_X_POS), can be negative)



> EXCEPTIONS

* if first byte of sprite sequence is `#$fe` (e.g. sprite_07), then the sprite is considered a "small sprite"
in this case, the sprite is made of a single entry. The entire byte sequence is 3 bytes long (including `#$fe`)
the second byte is the pattern table tile, and the third byte is the sprite attributes
the X position is set to `#$fc` (-4 decimal) and the Y position is set to `#$f8` (-8 decimal).


* 如果精灵4byte的首byte是`#$fe`,比如sprite_07,则这个精灵看做小精灵,
这种情况的精灵只包含3个byte(连`#$fe`在内),第二个byte是贴图索引,第三个byte是属性,
X位置看做是`#$fc`,Y位置看做是`#$f8`,大概位置(-4,-8)左下半

* if the first byte of a group sequence is #$80 (shared sprite), then the next two bytes are a CPU address to continue reading at.
this address is then read to get the sprite CPU read address.
this allows sprites to share parts of each other

* 如果精灵4byte的首byte是`#$80`,则接下来的2个byte是地址信息,用来共享精灵数据
比如sprite_05